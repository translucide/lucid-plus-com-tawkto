<?php
/**
 * Collections of tasks to perform on install
 **/
$info = [
    'info' => [
        'title' => 'TawkTo Integration',
        'description' => 'Enables TakwTo Chat widget',
        'author' => 'Emmanuel Morin',
        'authorurl' => 'https://translucide.ca',
        'company' => 'Translucide',
        'companyurl' => 'https://translucide.ca',
        'url' => '',
        'version' => '0.1'
    ],
    'setting' => [
        [
            'category' => 'general',
            'name' => 'tawktositeid',
            'value' => '',
            'type' => 'text',
            'options' => '',
            'title' => 'TAWKTO_SITEID',
            'description' => 'TAWKTO_SITEID_DSC'
        ],
        [
            'category' => 'general',
            'name' => 'tawktoblacklist',
            'value' => '',
            'type' => 'textarea',
            'options' => '',
            'title' => 'TAWKTO_URLBLACKLIST',
            'description' => 'TAWKTO_URLBLACKLIST_DSC'
        ],
    ]
];
