<?php
global $service;
$service->get('EventHandler')->on('sys.onRender',
	function($e,$p){
		global $service;
		if ($service->get('Url')->isAdmin() == false) {
            $siteid = $service->get('Setting')->get('tawkto_tawktositeid');
            $url = $service->get('Url')->get();
            $found = false;
            $blacklist = explode("\n",$service->get('Setting')->get('tawkto_tawktoblacklist'));
            foreach($blacklist as $v) {
                if (trim($v) && strpos(URL.$service->get('Language')->getCode().'/'.$url['path'],$v) !== false) {
                    $found = true;
                }
            }
			if ($siteid != '' && !$found) {
				$service->get('Ressource')->addScript('var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();(function(){var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];s1.async=true;s1.src=\'https://embed.tawk.to/'.$siteid.'/default\';s1.charset=\'UTF-8\';s1.setAttribute(\'crossorigin\',\'*\');s0.parentNode.insertBefore(s1,s0);})();');
			}
		}
		return $p;
	}
);
?>
