<?php
define('TAWKTO_SITEID','Tawk.to: ID du Site');
define('TAWKTO_SITEID_DSC','Depuis le tableau de bord Tawk.to, allez à Site &amp; Pages, chisir Manage sites. Cliquez le bouton Manage sous la colonne Settings. Copier coller l\'identifiant de site ici.');
define('TAWKTO_URLBLACKLIST','Tawk.to: Liste noire d\'affichage');
define('TAWKTO_URLBLACKLIST_DSC','Une URL par ligne. Les URLS qui contiennent le texte sur une des lignes ne chargeront pas le chat.');
?>
